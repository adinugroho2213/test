'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
sass.compiler = require('node-sass');
 
gulp.task('sass', function () {
  return gulp.src('gulp/test/style.scss')
    .pipe(sass({
        errLogToConsole:true,
    })).on('error', console.error.bind(console))
    .pipe(gulp.dest('gulp/dest/css'));
});
 
